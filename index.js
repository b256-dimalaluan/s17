/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

function firstFunc() {
	let fullNameUser = prompt("What is your name?");
	let ageUser = prompt("How old are you?");
	let addressUser = prompt("Where do you live? ");

	console.log("Hello, " + fullNameUser + ".");
	console.log("You are " + ageUser + ".");
	console.log("You live in " + addressUser ".");
}

firstFunc();



/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

function myFavoriteBands(){
	console.log("1. My Chemical Romance");
	console.log("2. Sum41");
	console.log("3. The Good Charlotte");
	console.log("4. Chicosci");
	console.log("5. Slapshock");
	}
myFavoriteBands();



/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
function myFavoriteMovies() {
	console.log("1. The Godfather");
	console.log("Rotten Tomatoes Rating: 97%");
	console.log("2. Infinity Pool");
	console.log("Rotten Tomatoes Rating: 87%");
	console.log("3. The Wolf of Wall Street ");
	console.log("Rotten Tomatoes Rating: 80%");
	console.log("4.EUROTRIP");
	console.log("Rotten Tomatoes Rating: 40%");
	console.log("5. American Pie");
	console.log("Rotten Tomatoes Rating: 61%");
}

myFavoriteMovies();




/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};


printUsers();